package com.agileengine.model;

public enum TransactionType {
	CREDIT ("CREDIT"),
	DEBIT ("DEBIT");
	
	private final String value;

	private TransactionType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}
