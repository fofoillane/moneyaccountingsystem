package com.agileengine.model;

public class User {
	
	private String firstName;
	private String lastName;
	private String username;
	private Account account;
	
	public User() {}
	
	public User(String firstName, String lastName, String username, Account account) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.account = account;
	}
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
