package com.agileengine.model;

public class Transaction {
	
	private TransactionType type;
	private double value;
	private User user;

	public Transaction() {}
	
	public Transaction(TransactionType type, double value, User user) {
		super();
		this.type = type;
		this.value = value;
		this.user = user;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Transaction [type=" + type + ", value=" + value + ", user=" + user + "]";
	}
}
