package com.agileengine.model;

import java.util.ArrayList;
import java.util.List;

public class Account {
	
	private double balance;
	private List<Transaction> transactionHistory;
	
	public Account() {
		this.transactionHistory = new ArrayList<Transaction>();
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public List<Transaction> getTransactionHistory() {
		return transactionHistory;
	}

	public void setTransactionHistory(List<Transaction> transactionHistory) {
		this.transactionHistory = transactionHistory;
	}

}