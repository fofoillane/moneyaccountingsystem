package com.agileengine.dao;

import com.agileengine.model.Account;
import com.agileengine.model.Transaction;
import com.agileengine.model.TransactionType;

public class TransactionDAO {

	public boolean doTransaction (Transaction transaction) {
		Account acct = transaction.getUser().getAccount();
		if(TransactionType.CREDIT.equals(transaction.getType())) {
			if(acct.getBalance() == 0.0)
				return false;
			else {
				double result = acct.getBalance() - transaction.getValue();
				if(result < 0.0)
					return false;
				else {
					acct.setBalance(result);
				}
			}
		} else {
			acct.setBalance(acct.getBalance() + transaction.getValue());
		}
		
		return true;
	}
}
