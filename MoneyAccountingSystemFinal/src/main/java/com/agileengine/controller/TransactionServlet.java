package com.agileengine.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agileengine.dao.TransactionDAO;
import com.agileengine.dao.UserDAO;
import com.agileengine.model.Account;
import com.agileengine.model.Transaction;
import com.agileengine.model.TransactionType;
import com.agileengine.model.User;

/**
 * Servlet implementation class TransactionServlet
 */
public class TransactionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TransactionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("views/doTransaction.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//get the values from the view
		String valueS = request.getParameter("value");
		String typeS = request.getParameter("type");
		String username = request.getParameter("username");
		
		//get the user
		UserDAO userDAO = new UserDAO();
		User user = userDAO.getUser(username);
		
		//get the balance from session and create a new account
		Account acct;
		if(request.getSession().getAttribute("acct") == null) {
			acct = new Account();
			acct.setBalance(0.0);
		}
		else
			acct = ((Account)request.getSession().getAttribute("acct"));
		
		user.setAccount(acct);
		
		//transform the values to be as excepted
		double value = Double.parseDouble(valueS);
		TransactionType type = TransactionType.valueOf(typeS);
		
		Transaction transaction = new Transaction(type, value, user);
		
		TransactionDAO transactionDAO = new TransactionDAO();
		boolean success = transactionDAO.doTransaction(transaction);
		
		RequestDispatcher dispatcher;
		
		if(success) {
			acct.getTransactionHistory().add(transaction);
			request.getSession().setAttribute("acct", user.getAccount());
			dispatcher = request.getRequestDispatcher("views/AccountBalance.jsp");
		} else
			dispatcher = request.getRequestDispatcher("views/TransactionError.jsp");
		
		dispatcher.forward(request, response);
	}

}
