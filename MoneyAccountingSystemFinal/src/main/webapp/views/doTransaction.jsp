<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div align="center">
		<h1>Transaction Form</h1>
		<form action="<%=request.getContextPath()%>/doTransaction" method="post">
			<table>
				<tr>
					<td>Type</td>
					<td>
						<select name="type" required="required">
							<option value="CREDIT">CREDIT</option>
							<option value="DEBIT">DEDIT</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Value</td>
					<td><input type="number" name="value"/></td>
				</tr>
				<tr>
					<td>Username</td>
					<td><input type="text" name="username"/></td>
				</tr>
			</table>
			<input type="submit" value="Submit"/>
		</form>
	</div>
</body>
</html>