<%@page import="com.agileengine.model.Transaction"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.agileengine.model.Account"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<%
		Account acct = (Account)request.getSession().getAttribute("acct"); 
	%>
<head>
<style>
table, th, td {
  border: 1px solid black;
}

th, td {
  padding: 10px;
}
</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div align="center">
		<h2>Your transaction was successfully processed!</h2>
		<br>
		<h3>The balance in your account is: $<% out.println(acct.getBalance());%></h3>
		<br>
		<br>
		<table>
		 <thead>
            <tr>
                <th>Type</th>
                <th>Value</th>
            </tr>
        </thead>
        <tbody>
		     <% for(int i = 0; i < acct.getTransactionHistory().size(); i++) {
		    	 Transaction transaction = new Transaction();
		    	 transaction = acct.getTransactionHistory().get(i);
            %>
            
             <tr>
                <td><%=transaction.getType()%></td>
                <td><%=transaction.getValue()%></td>
               </tr>
            <%
            };
            %>
        </tbody>
		</table>
		<br>
		<button type="button" name="back" onclick="history.back()">Back</button>
	</div>
</body>
</html>